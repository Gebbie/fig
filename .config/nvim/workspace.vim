" Navigating buffers
nnoremap <A-n> :bn<CR>
nnoremap <A-p> :bp<CR>
" Switch buffer prompt
nnoremap <Leader>b :ls<CR>:b<Space>
" Edit file prompt
nnoremap <Leader>e :e<Space>
" View file prompt
nnoremap <Leader>r :view<Space>
" Buffer actions
nnoremap <A-w> :w<CR>
nnoremap <A-q> :q<CR>
nnoremap <A-Q> :q!<CR>
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>
nnoremap <Leader>Q :q!<CR>
" Close buffer but not window
nnoremap <Leader>x :bp<Bar>bd #<CR>

" Opening windows
set splitbelow
set splitright
nnoremap <A-s> :split<CR>
nnoremap <A-v> :vsplit<CR>
nnoremap <Leader>s :split<CR>
nnoremap <Leader>v :vsplit<CR>
" Navigating windows (in lots of modes)
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l
nnoremap <A-Left> <C-w>h
nnoremap <A-Down> <C-w>j
nnoremap <A-Up> <C-w>k
nnoremap <A-Right> <C-w>l
inoremap <A-h> <ESC><C-w>h
inoremap <A-j> <ESC><C-w>j
inoremap <A-k> <ESC><C-w>k
inoremap <A-l> <ESC><C-w>l
inoremap <A-Left> <ESC><C-w>h
inoremap <A-Down> <ESC><C-w>j
inoremap <A-Up> <ESC><C-w>k
inoremap <A-Right> <ESC><C-w>l
tnoremap <A-Left> <C-\><C-n><C-w>h
tnoremap <A-Down> <C-\><C-n><C-w>j
tnoremap <A-Up> <C-\><C-n><C-w>k
tnoremap <A-Right> <C-\><C-n><C-w>l
" Cycling round windows
nnoremap <A-c> <C-w>w
inoremap <A-c> <ESC><C-w>w
tnoremap <A-c> <C-\><C-n><C-w>w
nnoremap <A-C> <C-w>W
inoremap <A-C> <ESC><C-w>W
tnoremap <A-C> <C-\><C-n><C-w>W

" Tabs
nnoremap <A-t> :tabnext<CR>
nnoremap <A-T> :tabprevious<CR>
