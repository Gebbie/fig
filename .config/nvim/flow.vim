" (Insert-mode)
" Using these after a C-p/n autocomplete lets you get the next words too
inoremap <A-p> <C-x><C-p>
inoremap <A-n> <C-x><C-n>
" Repeat word plus a space
inoremap <A-a> <Space><C-p>
" Repeat word plus a space and increment a number in it
" (The temporary appended 'X' stops C-p ignoring lone chars)
inoremap <A-i> X<Space><C-p><ESC>xhgex<C-a>a
" Autocomplete a filename
inoremap <C-f> <C-x><C-f>

" Clone lines commentting them out (using vim-commentary)
nmap gcp yygc']']p
nmap gcP yygc']P
" Clone lines
nnoremap <A-b> yy']p
nnoremap <A-B> yyP
" Create count blank lines below/above
nnoremap <silent> <A-o> :<C-u>put =repeat(nr2char(10),v:count)<Bar>execute "'[-1"<CR>
nnoremap <silent> <A-O> :<C-u>put!=repeat(nr2char(10),v:count)<Bar>execute "']+1"<CR>
" Swap the current and next word
nnoremap gw WdaWBPB

" Clipboard
map <Leader>y "+y
map <Leader>Y "+Y
map <Leader>p "+p
map <Leader>P "+P
map <Leader>gp "+gp
map <Leader>gP "+gP

" Opens a :s with the pattern and the range both specified by motions
" gs{pattern-motion}{range-motion}
nnoremap <silent> gs :set opfunc=MotionSubPart1<CR>g@
function! MotionSubPart1(type, ...)
	call feedkeys("`[v`]\"9y:set opfunc=MotionSubPart2g@")
endfunction
function! MotionSubPart2(type, ...)
	call feedkeys(":'[,']s/\\V9/g\<Left>\<Left>/")
endfunction
" Same as above but using vim-abolish's :Subvert
nnoremap <silent> gS :set opfunc=MotionSubvertPart1<CR>g@
function! MotionSubvertPart1(type, ...)
	call feedkeys("`[v`]\"9y:set opfunc=MotionSubvertPart2g@")
endfunction
function! MotionSubvertPart2(type, ...)
	call feedkeys(":'[,']S/9/g\<Left>\<Left>/")
endfunction

" Toggle cursor going where there are no characters
nnoremap <silent> <Leader>c :<C-u>exec 'set virtualedit=' . (&virtualedit == '' ? 'all' : '')<CR>
vnoremap <silent> <Leader>c :<C-u>exec 'set virtualedit=' . (&virtualedit == '' ? 'all' : '')<CR>gv

" Execute lines as VimScript
nnoremap <A-e> yy:@"<CR>

" Mappings I use to send lines to a REPL in a :terminal window
" (The :terminal window has to be on the right)
nnoremap <A-r> yy<C-w>lpA<CR><C-\><C-n><ESC><C-w>p
vnoremap <A-r> y<C-w>lpA<CR><C-\><C-n><ESC><C-w>p
inoremap <A-r> <ESC>yy<C-w>lpA<CR><C-\><C-n><ESC><C-w>pgi
" Send the last selection
nmap <A-R> gv<A-r>
