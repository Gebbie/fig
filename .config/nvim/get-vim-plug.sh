#!/usr/bin/env sh
url=https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
dir="${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/autoload/plug.vim"
[ -e "$dir" ] && exit 1
curl -fLo "$dir" --create-dirs "$url"
