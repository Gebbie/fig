" Set <Leader> in mappings to be Space
let mapleader=" "

runtime plugins.vim
runtime basic.vim
runtime flow.vim
runtime workspace.vim
runtime experimental.vim
runtime cheeky.vim
