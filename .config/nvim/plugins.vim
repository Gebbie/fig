" Install vim-plug if not already
call system('"${XDG_CONFIG_HOME:-$HOME/.config}/nvim/get-vim-plug.sh"')
if v:shell_error == 0
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
" ysiw) (surround word with brackets)
Plug 'https://github.com/tpope/vim-surround'
" Used by plugins so their actions undo and repeat atomically
Plug 'https://github.com/tpope/vim-repeat'
" ci" (change in quotes)
Plug 'https://github.com/wellle/targets.vim'
" gcc to comment/uncomment lines
Plug 'https://github.com/tpope/vim-commentary'
" Subbing/searching variants like box,boxes,Box,Boxes
" Insert-mode case changing myVar -> MY_VAR -> my-var
Plug 'https://github.com/tpope/vim-abolish'
" Move,Rename,Chmod,SudoWrite
Plug 'https://github.com/tpope/vim-eunuch'
" Search for selected with *
Plug 'https://github.com/nelstrom/vim-visual-star-search'
" Syntax highlighting for sxhkd
Plug 'https://github.com/baskerville/vim-sxhkdrc'
call plug#end()

" vim-commentary
" Set the default to '# '
set commentstring=#\ %s
augroup commentary
	autocmd!
	" Use // not /* */
	autocmd FileType c,cpp,cs,java,arduino setlocal commentstring=//\ %s
	" Add Forth \
	autocmd FileType forth setlocal commentstring=\\\ %s
augroup END
