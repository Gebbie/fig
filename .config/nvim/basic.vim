set number relativenumber
set noexpandtab
set tabstop=4
set shiftwidth=4
" Set C-w and C-u to ignore where Insert-mode was entered (by default they stop there)
set backspace=3

" Dull status-lines of not-current windows
highlight StatusLineNC ctermfg=darkgray

" Y delete to end of line
nnoremap Y y$
" U redo
nnoremap U <C-r>
" Toggle search highlight
nnoremap <expr> <Leader>h (&hls && v:hlsearch ? ':nohls' : ':set hls')."\n"

" Get outcome previews for (some) commands, good with :s
set inccommand=split

augroup basic
	autocmd!
	" Disable automatic comment continuation on newline
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
	" .h to C (default C++)
	autocmd BufRead,BufNewFile *.h set filetype=c
augroup END

" Ctrl+BackSpace delete word in Insert-mode
noremap! <C-h> <C-w>
" Ctrl+Left/Right back/forward word in Insert-mode
map <ESC>[Od <C-left>
map <ESC>[Oc <C-right>
inoremap <C-left> <C-o>b
" Disable PageDown and PageUp in Insert-mode
inoremap <PageDown> <nop>
inoremap <PageUp> <nop>
" Home like ^ not 0
nnoremap <Home> ^

" [count]v/V/<C-v> select that many chars/lines/cells
nnoremap <expr> v '@_v"_y' . v:count1 . "v"
nnoremap <expr> V "@_V" . (v:count>1 ? (v:count-1)."j" : "")
nnoremap <expr> <C-v> "@_\<C-v>" . (v:count>1 ? (v:count-1)."j" : "")

" Escape key exit Terminal-mode (Insert-mode equivalent in :terminal)
:tnoremap <Esc> <C-\><C-n>

augroup neovim_terminal
    autocmd!
    autocmd TermOpen * :setlocal nonumber norelativenumber
    autocmd TermOpen * nnoremap <buffer> <C-c> i<C-c>
augroup END
