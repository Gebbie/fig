# Change what's considered a word
# dir/ becomes one word not two
autoload -U select-word-style
select-word-style bash

# Source aliases
. "$ME/aliases"
[ -e "$ME/aliases2" ] && . "$ME/aliases2"
# Source plugins
{ . /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh || . /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh; } 2>/dev/null

# If launched elsewhere than $HOME print the path
[[ "$PWD" != "$HOME" ]] && print "\e[$((COLUMNS/2 - ${#PWD}/2))G$PWD"

# Command completion
autoload -Uz compinit
compinit
# Command correction prompt
setopt correct
SPROMPT="zsh: correct %{$fg[red]%}%R%{$reset_color%} to %{$fg[green]%}%r%{$reset_color%}? [nyae] "

# Vi mode
bindkey -v
KEYTIMEOUT=1  # remove delay entering normal mode

# Make echo need -e to interpret backslash escapes
setopt bsdecho

# Change prompt and cursor for vi modes
autoload -U colors && colors
prompt_ins="$ "
prompt_norm="%{$fg[red]%}$%{$reset_color%} "
zle -N zle-line-init
zle -N zle-keymap-select
function zle-line-init zle-keymap-select {
	PS1="${${KEYMAP/vicmd/${prompt_norm}}/(main|viins)/${prompt_ins}}"
	zle reset-prompt
	[ "$KEYMAP" = vicmd ] && printf '\e[2 q' || printf '\e[6 q'
}

# History
HISTFILE="${XDG_DATA_HOME:-$HOME/.local/share}/zsh/history"
mkdir -p "${HISTFILE%/*}"
HISTSIZE=10000
SAVEHIST=10000
setopt HIST_IGNORE_DUPS
setopt HIST_REDUCE_BLANKS

# Get special keys
typeset -g -A key
key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[ShiftTab]="${terminfo[kcbt]}"
# Set special keys
[[ -n "${key[Home]}" ]] && bindkey -- "${key[Home]}" beginning-of-line
[[ -n "${key[Home]}" ]] && bindkey -M vicmd -- "${key[Home]}" beginning-of-line
[[ -n "${key[End]}" ]] && bindkey -- "${key[End]}" end-of-line
[[ -n "${key[End]}" ]] && bindkey -M vicmd -- "${key[End]}" end-of-line
[[ -n "${key[Insert]}" ]] && bindkey -- "${key[Insert]}" overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}" backward-delete-char
[[ -n "${key[Delete]}" ]] && bindkey -- "${key[Delete]}" delete-char
[[ -n "${key[Left]}" ]] && bindkey -- "${key[Left]}" backward-char
[[ -n "${key[Right]}" ]] && bindkey -- "${key[Right]}" forward-char
[[ -n "${key[PageUp]}" ]] && bindkey -- "${key[PageUp]}" beginning-of-buffer-or-history
[[ -n "${key[PageDown]}" ]] && bindkey -- "${key[PageDown]}" end-of-buffer-or-history
[[ -n "${key[ShiftTab]}" ]] && bindkey -- "${key[ShiftTab]}" reverse-menu-complete

# Unbind : vicmd from execute-named-cmd (annoying 'execute:' prompt)
bindkey -a -r :
# Have Tab expansion only look behind the cursor
bindkey '^I' expand-or-complete-prefix
# U vicmd redo
bindkey -a U redo

# Ctrl+BackSpace/Delete delete word
bindkey '^H' backward-kill-word
bindkey '^[[3;5~' kill-word
# Ctrl+Left/Right back/forward word
bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word
# Alt+Left/Right back/forward WORD
bindkey '^[[1;3D' vi-backward-blank-word
bindkey '^[[1;3C' vi-forward-blank-word

# History substring search
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
[[ -n "${key[Up]}" ]] && bindkey -- "${key[Up]}" up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search
bindkey '^[k' up-line-or-beginning-search
bindkey '^[j' down-line-or-beginning-search

function is_single_line {
	[[ "$CONTEXT" != cont ]] && [[ $BUFFER != *$'\n'* ]]
}
# Take currently typed command to a new command line
# TODO Check out pushln in man zshbuiltins
function pull_command {
	is_single_line || return 1
	zle push-input
	zle -R
	zle accept-line
}

bindkey '^[a' copy-prev-shell-word  # Alt+a
# Rebind vi Ctrl+w and Ctrl+u to ignore where you entered insert mode
bindkey '^W' backward-kill-word
bindkey '^U' backward-kill-line

# Copy buffer to X clipboard keybinding
function clipboard-copy-buffer {
	print -rn -- "$BUFFER" | xclip -selection clipboard
}
zle -N clipboard-copy-buffer
bindkey '^[y' clipboard-copy-buffer  # Alt+y

# ls without dotfiles keybinding
function myls {
	pull_command || return 1
	local x="$(ls --color=always --format=vertical --width="$COLUMNS" --quoting-style=shell)"
	printf "${x:+\n}%s" "$x"
}
zle -N myls
bindkey '^[l' myls  # Alt+l
# ls with dotfiles keybinding
function mylsdots {
	pull_command || return 1
	local x="$(ls --almost-all --color=always --format=vertical --width="$COLUMNS" --quoting-style=shell)"
	printf "${x:+\n}%s" "$x"
}
zle -N mylsdots
bindkey '^[L' mylsdots  # Alt+L

# pwd abbreviated keybinding
function mypwdabbr {
	pull_command || return 1
	printf %s "${${PWD/#%$HOME/~}/#$HOME\//~/}"
}
zle -N mypwdabbr
bindkey '^[w' mypwdabbr  # Alt+w
# pwd not abbreviated keybinding
function mypwd {
	pull_command || return 1
	printf %s "$PWD"
}
zle -N mypwd
bindkey '^[W' mypwd  # Alt+W
