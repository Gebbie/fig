export ME="${XDG_CONFIG_HOME:-$HOME/.config}/me"
. "$ME/envs"
[ -e "$ME/envs2" ] && . "$ME/envs2"
[ -z "$DISPLAY" ] && [ "$(fgconsole)" = 1 ] && exec startx
